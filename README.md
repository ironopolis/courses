# PACKAGE DEVELOPMENT
This repo contains a basic outline of a package. It demostrates:
- local composer.json configuration;
- autoloading namespaced classes;
- publishing assets;
- running migrations;
- accessing models from controllers;
- registering routes

# INSTALLING YOUR PACKAGE LOCALLY
To prevent having to pull changes from git continuously it makes sense, when developing locally, to use a symlink to pull your package in to Vendor from a directory on your machine. **NOTE:** You should still work on a branch and commit changes/ push to GitLab during development ensure your work is backed up and that your git history is modular.

To achieve this your "repositories" block should reflect this format:

```
 "repositories": [
        {
            "type": "path",
            "url": "./packages/ironopolis/homepage",
            "options": {
                "symlink": true
            }
        }
    ],
```


# INSTALLING YOUR PACKAGE REMOTELY
In production, will install our pacakges directly from its GitLab repo via the composer.json file:

```
 "repositories": [
        {
            "type": "vcs",
            "url":  "git@gitlab.com:ironopolis/homepage.git"
        },
        {
            "type": "vcs",
            "url":  "git@gitlab.com:ironopolis/courses.git"
        }
    ],
```

# RESOURCES

Some useful resources are:
- https://laravel.com/docs/5.6/packages (Offical Docs)
- https://laravel-news.com/5-part-tutorial-series-on-creating-a-laravel-5-package
- Demo package on GitHub https://github.com/websanova/laravel-demo/tree/master/src
