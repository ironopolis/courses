<?php

namespace Ironopolis\Course;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'title', 'slug','description', 'price', 'published', 'exam_price', 'duration',
        'assessment_method', 'minimum_requirements', 'image_path', 'resource_path', 
        'start', 'end', 'parent', 'published'
    ];
}
