<?php

namespace Ironopolis\Course;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Ironopolis\Course\Course;
use Ironopolis\Course\Module;
use Request;

class CourseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \View::composer('*', function ($view) {
            if (Request::path() == 'courses') {
                $modules = Module::all();
                //$courses = Course::where('parent', $module->id);
                $data['hero']['active'] = true;
                $data['hero']['hero'][0]['title'] = 'Training';
                foreach ($modules as $module) {
                    $data['modules'][$module->id]['id'] = $module->id;
                    $data['modules'][$module->id]['title'] = $module->title;
                    $data['modules'][$module->id]['slug'] = $module->slug;
                    $data['modules'][$module->id]['active'] = false;
                    $data['modules'][$module->id]['description'] = $module->description;
                    $data['modules'][$module->id]['courses'] = Course::where('parent', $module->id)->get();
                }
                $view->with('data', json_encode($data));
            }
        });
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'courses');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->publishes([
            __DIR__.'/resources/assets' => base_path('resources/assets/packages/courses'),
        ], 'public');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
