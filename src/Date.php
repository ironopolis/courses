<?php

namespace Ironopolis\Course;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'code', 'datetime'
    ];
}
