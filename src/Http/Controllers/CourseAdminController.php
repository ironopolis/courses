<?php

namespace Ironopolis\Course\Http\Controllers;

use App\Http\Controllers\Controller;
use Ironopolis\Course\Course;
use Ironopolis\Course\Module;
use Ironopolis\Course\Date;
use Ironopolis\Skeleton\Body;
use Illuminate\Http\Request;
use League\Csv\Reader;

class CourseAdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
       //$this->middleware('auth');
    }

    public function index()
    {
        $modules = Module::all();
        $courses = Course::paginate(10);
        return view('courses::admin.courses', ['courses' => $courses, 'modules' => $modules]);
    }

    public function search(Request $request)
    {
        $results['results'] = Course::where('title', $request->query('query'))
            ->orWhere('title', 'like', '%' . $request->query('query') . '%')->get();
        return json_encode($results);
    }

    public function build()
    {
        $courses = Course::all();
        if (!empty($courses)) {
            foreach($courses as $course) {
                $course_json = null;
                $course_json['course'] = [
                    'title' => $course->title,
                    'slug' => $course->slug,
                    'description' => $course->description,
                    'price' => $course->price,
                    'exam_price' => $course->exam_price,
                    'duration' => $course->duration,
                    'resource_path' => $course->resource_path,
                    'awarding_body' => $course->awarding_body,
                    'assessment_method' => $course->assessment_method,
                    'minimum_requirements' => $course->minimum_requirements,
                    'image_path' => $course->image_path,
                    'resource_path' => $course->resource_path,
                ];
                $module = Module::where('id', $course->parent)->first();
                $course_json['hero']['active'] = true;
                $course_json['hero']['hero'][] = [
                    'title' => $module->title,
                    'subtitle' => $module->description,
                ];    
                Body::create([
                    'route' => $course->slug,
                    'data' => json_encode($course_json),
                    'type' => 'course'
                ]);            
            }
        }     
        dd('Courses Imported');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('courses::module-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        $dates = Date::select('datetime')->where('code', $course->code)->get();
        $encode_dates = [];
        foreach ($dates as $date) {
            $encode_dates[] = $date['datetime'];
        }
        $course->dates = json_encode($encode_dates);
        return view('courses::admin.course', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        //
    }

    public function getImport()
    {
        return view('courses::admin.import');
    }

    public function getDatesImport()
    {
        return view('courses::admin.dates_import');
    }

    public function upload(Request $request)
    {
        if ($request->file('courses')) {
            $path = $request->file('courses')->getRealPath();
            $reader = Reader::createFromPath($path, 'r');
            $csv_data = $reader->fetchAll();
            $header = array_slice($csv_data, 0, 1);
            unset($csv_data[0]);
            $csv_header_fields = [];
            foreach ($header[0] as $key => $value) {
                $csv_header_fields[] = $value;
            }
            foreach ($csv_data as $index => $row) {
                foreach ($row as $rowindex => $entry) {
                    $courses[$index][$csv_header_fields[$rowindex]] = $entry;
                }
            }
            foreach ($courses as $course) {
                Course::updateOrCreate(
                    ['code' => $course['code']],
                    [
                        'code' => $course['code'],
                        'slug' => $course['slug'],
                        'title' => $course['title'],
                        'description' => $course['description'],
                        'price' => (float) $course['price'], 
                        'exam_price' => (float) $course['exam_price'], 
                        'duration' => (float) $course['duration'],
                        'awarding_body' => $course['awarding_body'],
                        'assessment_method' => $course['assessment_method'], 
                        'minimum_requirements' => $course['minimum_requirements'], 
                        'image_path' => $course['image_path'], 
                        'resource_path' => $course['resource_path'], 
                        'parent' => (int) $course['parent'], 
                        'published' => (int) $course['published']
                    ]
                ); 
            }
            dd('Courses Updated');
        }
    }
    public function uploadDates(Request $request)
    {
        if ($request->file('courses')) {
            $path = $request->file('courses')->getRealPath();
            $reader = Reader::createFromPath($path, 'r');
            $csv_data = $reader->fetchAll();
            $header = array_slice($csv_data, 0, 1);
            unset($csv_data[0]);
            Date::truncate();     
            foreach ($csv_data as $date) {
                $bits = explode('/',$date[2]);
                $formatted_date = $bits[1].'/'.$bits[0].'/'.$bits[2];
                Date::create(['code' => $date[1], 'datetime' => date("Y-m-d H:i:s", strtotime($formatted_date))]);
            }
            dd('Dates Added');
        }
    }
}
