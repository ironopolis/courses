<?php

namespace Ironopolis\Course\Http\Controllers;

use App\Http\Controllers\Controller;
use Ironopolis\Course\Course;
use Ironopolis\Course\Module;
use Ironopolis\Skeleton\Body;
use Ironopolis\Skeleton\FormSubmission;
use Illuminate\Http\Request;
use Ironopolis\Course\Date;
use Ironopolis\Course\Mail\CourseBooking;
use Mail;

class CourseController extends Controller
{
    public function __construct(Request $request) {
       //$this->middleware('auth');
    }
    public function index() {
        return view('courses::courses');
    }
    public function show(Course $course) {
        $dates = Date::select('datetime')->where('code', $course->code)->get();
        $encode_dates = [];
        foreach ($dates as $date) {
            $encode_dates[] = $date['datetime'];
        }
        $course->dates = json_encode($encode_dates);
        dd($course);

        $course_json['course'] = $course;
        $module = Module::where('id', $course->parent)->first();
        $course_json['hero']['blocks'] = [
            'title' => $module->title,
            'subtitle' => $module->description,
            'active' => true
        ];  
        $modules = Module::all();
        foreach ($modules as $module) {
            $data['modules'][$module->id]['id'] = $module->id;
            $data['modules'][$module->id]['title'] = $module->title;
            $data['modules'][$module->id]['slug'] = $module->slug;
            $data['modules'][$module->id]['image'] = $module->image_path;
            $data['modules'][$module->id]['active'] = false;
            $data['modules'][$module->id]['description'] = $module->description;
            $data['modules'][$module->id]['courses'] = Course::where('parent', $module->id)->get();
        }
        return view('courses::course', ['course' => json_encode($course_json), 'modules' => $data, 'module' => $moduleclass]);
    }
    public function find($slug) {
        $course_json = Body::where('route', $slug)->first();

        $decoded = json_decode($course_json->data);
        $decoded_course = $decoded->course;
        $dates = Date::select('datetime')->where('code', $decoded_course->code)->get();
        $encode_dates = [];
        foreach ($dates as $date) {
            $encode_dates[] = $date['datetime'];
        }
        $decoded_course->dates = json_encode($encode_dates);
        $decoded->course = $decoded_course;
        $encoded = json_encode($decoded);

        $course = Course::select('parent')->where('slug', $slug)->first();
        $modules = Module::all();
        foreach ($modules as $module) {
            if ($module->id == $course->parent) {
                $moduleclass = $module->slug;
            }
            $data['modules'][$module->id]['id'] = $module->id;
            $data['modules'][$module->id]['title'] = $module->title;
            $data['modules'][$module->id]['slug'] = $module->slug;
            $data['modules'][$module->id]['image'] = $module->image_path;
            $data['modules'][$module->id]['active'] = false;
            $data['modules'][$module->id]['description'] = $module->description;
            $data['modules'][$module->id]['courses'] = Course::where('parent', $module->id)->get();
        }
        return view('courses::course', ['course' => $encoded, 'modules' => json_encode($data), 'module' => $moduleclass]);
    }
    public function submission(Request $request) {
        $this->validate(
            $request,
            [
                'accept' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'name' => 'required'
            ]
        );
        $submission = FormSubmission::create(
            [
                'type' => 'booking',
                'data' => json_encode($request->except('_token'))
            ]
        );
        $content = [
            "code" => $request->input('code'),
            "attendees" => !empty($request->input('attendees')) ? $request->input('attendees') : 'Group Enquiry',
            "group" => !empty($request->input('group')) ? 'Yes' : 'No',
            "requested_date" => $request->input('requested_date'),
            "name" => $request->input('name'),
            "course" => $request->input('course'),
            "company" => $request->input('company'),
            "address1" => $request->input('address1'),
            "address2" => $request->input('address2'),
            "address3" => $request->input('address3'),
            "postcode" => $request->input('postcode'),
            "phone" => $request->input('phone'),
            "email" => $request->input('email'),
            "accounts" => !empty($request->input('accounts')) ? 'Yes' : 'No',
            "newsletter" => !empty($request->input('newsletter')) ? 'Yes' : 'No'
        ];
        $email = new CourseBooking(
            $content
        );
        Mail::to('info@elecsafety.co.uk')->send($email);
        return redirect('/success');
    }
}
