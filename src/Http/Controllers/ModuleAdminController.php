<?php

namespace Ironopolis\Course\Http\Controllers;

use App\Http\Controllers\Controller;
use Ironopolis\Course\Course;
use Ironopolis\Course\Module;
use Ironopolis\Skeleton\Body;
use DB;
use Illuminate\Http\Request;

class ModuleAdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
       $this->middleware('web');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Module::paginate(10);
        return view('courses::admin.modules', compact('modules'));
    }

     /**
     * Display the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        return view('courses::admin.module', compact('module'));
    }

    /**
     * Import modules and associated course data.
     *
     * @return \Illuminate\Http\Response
     */
    public function import()
    {

        //Import Modules
        // $modules = DB::table('wp_terms  as wp1')
        //             ->join('wp_term_taxonomy as wp2', 'wp1.term_id', '=', 'wp2.term_id')
        //             ->select('wp1.term_id', 'wp1.name', 'wp2.description')
        //             ->where([
        //                 ['taxonomy', '=','product_cat'],
        //                 ['parent', '=','0']
        //             ])
        //             ->get()->toArray();
        // foreach($modules as $module) {
        //     Module::create([
        //         'id' => $module->term_id,
        //         'title' => $module->name,
        //         'description' => $module->description
        //     ]); 
        // }
        //Import Courses
        $courses = DB::table('wp_terms  as wp1')
                    ->join('wp_term_taxonomy as wp2', 'wp1.term_id', '=', 'wp2.term_id')
                    ->select(
                        'wp2.term_id', 
                        'wp1.name', 
                        'wp1.slug', 
                        'wp2.description',
                        'wp2.cost', 
                        'wp2.exam', 
                        'wp2.duration', 
                        'wp2.assessment', 
                        'wp2.requirements', 
                        'wp2.download', 
                        'wp2.parent'
                    )
                    ->where([
                        ['taxonomy', '=','product_cat'],
                        ['parent', '<>','0']
                    ])
                    ->get()
                    ->toArray();
        foreach ($courses as $course) {
            $path = explode('/', $course->download);
            $download = '';
            if (!empty($path[6])) {
                $download = '/'.$path[6];
            }
            Course::create([
                'slug' => $course->slug,
                'title' => $course->name,
                'description' => $course->description,
                'price' => $course->cost, 
                'exam_price' => $course->exam, 
                'duration' => $course->duration,
                'assessment_method' => $course->assessment, 
                'minimum_requirements' => $course->requirements, 
                'image_path' => null, 
                'resource_path' => $download, 
                'parent' => $course->parent, 
                'published' => false
            ]); 
        }
        dd('Import Complete');
    }

    /**
     * Display settings that relate to Modules.
     *
     * @return \Illuminate\Http\Response
     */
    public function build()
    {
        $data = Body::where('route', 'modules')->first();
        if (empty($data)) {
            $data = 'undefined';
        } else {
            $data = $data->data;
        }
        //dd($data);
        return view('courses::modules-builder', [ 'module' => $data ]);
    }

    /**
     * Save settings that relate to Modules.
     *
     * @return \Illuminate\Http\Response
     */
    public function save()
    {
        dd('here');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('courses::module-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->input());
        $this->validate($request, [
            'title' => ['required'],
            'image' => ['required', 'image']
        ]);
        $module = Module::create([
            'title' => request('title'),
            'description' => request('description'),
            'image_path' => $request->file('image')->store('modules'),
            'published' => $request->has('published') ? 1 : 0,
        ]);          
        return back();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        //
    }
}
