<?php

namespace Ironopolis\Course\Http\Controllers;

use App\Http\Controllers\Controller;
use Ironopolis\Course\Course;
use Ironopolis\Course\Module;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
       //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Module::all();
        $data['hero']['active'] = true;
        $data['hero']['blocks'][0]['title'] = 'Training';
        foreach ($modules as $module) {
            $data['modules'][$module->id]['id'] = $module->id;
            $data['modules'][$module->id]['title'] = $module->title;
            $data['modules'][$module->id]['slug'] = $module->slug;
            $data['modules'][$module->id]['image'] = $module->image_path;
            $data['modules'][$module->id]['active'] = false;
            $data['modules'][$module->id]['description'] = $module->description;
            $data['modules'][$module->id]['courses'] = Course::where('parent', $module->id)->get();
        }
        return view('courses::modules', ['modules' => json_encode($data)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        $courses = Course::where('parent', $module->id)->get();
        $data['hero']['active'] = true;
        $data['hero']['blocks'][0]['title'] = $module->title;
        $class = $module->slug;
        foreach ($courses as $module) {
            $temp[$module->id]['id'] = $module->id;
            $temp[$module->id]['title'] = $module->title;
            $temp[$module->id]['slug'] = $module->slug;
            $temp[$module->id]['active'] = false;
            $temp[$module->id]['description'] = $module->description;
            $temp[$module->id]['price'] = $module->price;
        }
        foreach ($temp as $item) {
            $data['courses'][] = $item;
        }
        $modules = Module::all();
        foreach ($modules as $module) {
            $data['modules'][$module->id]['id'] = $module->id;
            $data['modules'][$module->id]['title'] = $module->title;
            $data['modules'][$module->id]['slug'] = $module->slug;
            $data['modules'][$module->id]['image'] = $module->image_path;
            $data['modules'][$module->id]['active'] = false;
            $data['modules'][$module->id]['description'] = $module->description;
            $data['modules'][$module->id]['courses'] = Course::where('parent', $module->id)->get();
        }
        return view('courses::module', ['modules' => json_encode($data), 'courses' => $courses, 'module' =>  $class]);
    }
}
