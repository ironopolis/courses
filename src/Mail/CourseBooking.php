<?php

namespace Ironopolis\Course\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CourseBooking extends Mailable
{
    use Queueable, SerializesModels;

    public $company;
    public $name;
    public $email;
    public $phone;
    public $code;
    public $course;
    public $attendees;
    public $group;
    public $requested_date;
    public $address1;
    public $address2;
    public $address3;
    public $postcode;
    public $accounts;
    public $newsletter;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fields)
    {
        $this->code = $fields['code'];
        $this->attendees = $fields['attendees'];
        $this->group = $fields['group'];
        $this->course = $fields['course'];
        $this->requested_date = $fields['requested_date'];
        $this->name = $fields['name'];
        $this->company = $fields['company'];
        $this->address1 = $fields['address1'];
        $this->address2 = $fields['address2'];
        $this->address3 = $fields['address3'];
        $this->postcode = $fields['postcode'];
        $this->accounts = $fields['accounts'];
        $this->email = $fields['email'];
        $this->phone = $fields['phone'];
        $this->newsletter = $fields['newsletter'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('courses::emails.booking');
    }
}
