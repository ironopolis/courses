<?php

namespace Ironopolis\Course;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [
        'id', 'title', 'description', 'image_path', 'published'
    ];
}
