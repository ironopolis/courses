<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->string('slug', 255);
            $table->string('title');
            $table->text('description');
            $table->decimal('price', 8, 2);
            $table->decimal('exam_price', 8, 2);
            $table->decimal('duration', 8, 2);
            $table->string('awarding_body', 255)->nullable();
            $table->text('assessment_method');
            $table->text('minimum_requirements');
            $table->string('image_path')->nullable();
            $table->string('resource_path')->nullable();
            $table->bigInteger('parent');
            $table->boolean('published')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
