Vue.component('modules-component', require('./components/ModulesComponent.vue'));
Vue.component('module-component', require('./components/ModuleComponent.vue'));
Vue.component('courses-component', require('./components/CoursesComponent.vue'));
Vue.component('course-component', require('./components/CourseComponent.vue'));
Vue.component('module-builder', require('./components/ModuleBuilderComponent.vue'));
Vue.component('module-editor', require('./components/ModuleEditor.vue'));
Vue.component('course-editor', require('./components/CourseEditor.vue'));
Vue.component('static-menu', require('./components/StaticMenuComponent.vue'));