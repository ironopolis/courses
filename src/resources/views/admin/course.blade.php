@extends('skeleton::layouts.admin')

@section('content')
<div class="container">
  <section class="section">
    <div class="container">
      <h1 class="title">Course</h1>
      <h2 class="subtitle">
        Edit a course.
      </h2>
      <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li><a href="/admin">Dashboard</a></li>
          <li><a href="/admin/courses">Courses</a></li>
          <li class="is-active"><a href="#" aria-current="page">Course</a></li>
        </ul>
      </nav>
      <form method="POST" action="/admin/course/store" enctype="multipart/form-data">
        @csrf
        <course-editor :coursedebug="true" :course="{{ $course }}"></course-editor>
      </form>
    </div>
  </section>
</div>
@endsection
