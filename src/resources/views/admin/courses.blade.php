@extends('skeleton::layouts.admin')

@section('content')

<section class="section">
  <div class="container">
  <div class="columns">
      <div class="column is-4">
        <nav class="breadcrumb" aria-label="breadcrumbs">
          <ul>
            <li><a href="/admin">Dashboard</a></li>
            <li class="is-active"><a href="#" aria-current="page">Courses</a></li>
          </ul>
        </nav>
      </div>
      <div class="column">
        <div class="field is-horizontal">
          <div class="field-label is-normal">
            <label class="label">Type</label>
          </div>
          <div class="field-body">
            <div class="field">
              <div class="select">
                <select>
                  <option>Filter by type</option>
                  @foreach ($modules as $module)
                  <option>{{ $module->title }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="column">
        <div class="field is-horizontal">
          <div class="field-label is-normal">
            <label class="label">Search</label>
          </div>
          <div class="field-body">
            <div class="field">
              <p class="control">
                <input class="input" type="text" placeholder="Search courses">
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box">
      {{ $courses->links() }}
    </div>
    <table class="table">
      <thead>
        <tr>
          <th><abbr title="Position">ID</abbr></th>
          <th>Course Name</th>
          <th>Course Image</th>
          <th>Published</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th><abbr title="Position">ID</abbr></th>
          <th>Course Name</th>
          <th>Course Image</th>
          <th>Published</th>
          <th>Actions</th>
        </tr>
      </tfoot>
      <tbody>
        @foreach($courses as $course)
        <tr>
          <th>{{ $course->id }}</th>
          <td><a href="/admin/course/{{ $course->id }}" title="{{ $course->title }}">{{ $course->title }}</td>
          <th><img width="200" src="{{ $course->image or 'https://bulma.io/images/placeholders/1280x960.png' }}" /></th>
          <td>{{ $course->published ? 'Published' : 'Draft' }}</td>
          <td><a class="delete"></a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div class="box">
      {{ $courses->links() }}
    </div>
  </div>
</section>
@endsection
