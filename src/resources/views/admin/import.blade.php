@extends('skeleton::layouts.admin')

@section('content')
    <div class="container">
        <section class="section">
            <div class="container">
            <h1 class="title">Course</h1>
            <h2 class="subtitle">
                Import courses via CSV.
            </h2>
            <form method="POST" action="/admin/courses/import" enctype="multipart/form-data">
                @csrf
                <div class="field">
                  <div class="file is-boxed is-danger">
                    <label class="file-label">
                      <input class="file-input" type="file" name="courses">
                      <span class="file-cta">
                        <span class="file-icon">
                          <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                            <path fill="#FFF" d="M14,13V17H10V13H7L12,8L17,13M19.35,10.03C18.67,6.59 15.64,4 12,4C9.11,4 6.6,5.64 5.35,8.03C2.34,8.36 0,10.9 0,14A6,6 0 0,0 6,20H19A5,5 0 0,0 24,15C24,12.36 21.95,10.22 19.35,10.03Z" />
                          </svg>
                        </span>
                        <span class="file-label">
                          Choose a file…
                        </span>
                      </span>
                    </label>
                  </div>
                </div>
                <input class="button is-danger" type="submit" value="Upload File">
            </form>
            </div>
        </section>
    </div>
@endsection