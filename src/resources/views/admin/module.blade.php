@extends('skeleton::layouts.admin')

@section('content')
<div class="container">
  <section class="section">
    <div class="container">
      <h1 class="title">Module</h1>
      <h2 class="subtitle">
        Edit a module.
      </h2>
      <form method="POST" action="/admin/module/store" enctype="multipart/form-data">
        @csrf
        <module-editor :blockdebug="true" :module="{{ $module }}"></module-editor>
      </form>
    </div>
  </section>
</div>
@endsection
