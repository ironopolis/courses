@extends('skeleton::layouts.admin')
<?php
//dd($module);
?>
@section('content')
<div class="container">
    <module-builder :modulebuilderdebug="true" :data="{{ $module }}"></module-builder>
</div>
@endsection