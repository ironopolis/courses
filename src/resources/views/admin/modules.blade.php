@extends('skeleton::layouts.admin')

@section('content')

<section class="section">
  <div class="container">
    <div class="box">
      {{ $modules->links() }}
    </div>
    <table class="table">
      <thead>
        <tr>
          <th><abbr title="Position">ID</abbr></th>
          <th>Module Name</th>
          <th>Module Image</th>
          <th>Published</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th><abbr title="Position">ID</abbr></th>
          <th>Module Name</th>
          <th>Module Image</th>
          <th>Published</th>
          <th>Actions</th>
        </tr>
      </tfoot>
      <tbody>
        @foreach($modules as $module)
        <tr>
          <th>{{ $module->id }}</th>
          <td><a href="/admin/module/{{ $module->id }}" title="{{ $module->title }}">{{ $module->title }}</td>
          <th><img width="200" src="{{ $module->image or 'https://bulma.io/images/placeholders/1280x960.png' }}" /></th>
          <td>{{ $module->published ? 'Published' : 'Draft' }}</td>
          <td><a class="delete"></a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div class="box">
      {{ $modules->links() }}
    </div>
  </div>
</section>
@endsection
