@extends('skeleton::layouts.app')

@section('content')
@if(count($errors))
<div class="notification is-danger" style="border-radius: 0px;margin-bottom: 0rem;">
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<course-component :settings="{{ json_encode($skeleton) }}" :coursepagedebug="false" :data="{{ $course }}" :menu="{{ $modules }}" :module="'{{ $module }}'" :old="{{ json_encode(old()) }}"></course-component>
@endsection