@extends('skeleton::layouts.app')

@section('content')

{{ $data or '' }}

<courses-component :coursespagedebug="false" :data="{{ $modules }}"></courses-component>

@endsection
