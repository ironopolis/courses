<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Course Booking Enquiry</title>
</head>

<body>

  <h1>Course Booking Enquiry</h1>

  <h2>Course Request Details</h2>

  <p><strong>Course: </strong>{{ $course }}</p>
  <p><strong>Code: </strong>{{ $code }}</p>
  <p><strong>Requested Date: </strong>{{ $requested_date }}</p>
  <p><strong>Attendees: </strong>{{ $attendees }}</p>
  <p><strong>Group Booking? </strong>{{ $group }}</p>

  <h2>Applicant Details</h2>

  <p><strong>Company: </strong>{{ $company }}</p>
  <p><strong>Name: </strong>{{ $name }}</p>
  <p><strong>Company: </strong>{{ $company }}</p>
  <p><strong>Address 1: </strong>{{ $address1 }}</p>
  <p><strong>Address 2: </strong>{{ $address2 }}</p>
  <p><strong>Address 3: </strong>{{ $address3 }}</p>
  <p><strong>Postcode: </strong>{{ $postcode }}</p>
  <p><strong>Email: </strong>{{ $email }}</p>
  <p><strong>Phone: </strong>{{ $phone }}</p>

  <h2>Misc</h2>

  <p><strong>Accounts Dept Paying? </strong>{{ $accounts }}</p>
  <p><strong>Newsletter Signup? </strong>{{ $newsletter }}</p>

</body>
</html>
