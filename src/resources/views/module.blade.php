@extends('skeleton::layouts.app')

@section('content')
<module-component :modulepagedebug="false" :data="{{ $modules }}" :module="'{{ $module }}'"></module-component>
@endsection
