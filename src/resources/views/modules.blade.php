@extends('skeleton::layouts.app')

@section('content')
<modules-component :modulepagedebug="false" :data="{{ $modules }}"></modules-component>
@endsection