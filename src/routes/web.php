<?php

//Front end module and course routes
Route::get('modules', 'Ironopolis\Course\Http\Controllers\ModuleController@index')->middleware('web');
Route::get('module/{module}', 'Ironopolis\Course\Http\Controllers\ModuleController@show')->middleware('web');
Route::get('courses', 'Ironopolis\Course\Http\Controllers\CourseController@index')->middleware('web');
Route::get('course/find/{course}', 'Ironopolis\Course\Http\Controllers\CourseController@show')->middleware('web');
Route::post('course/submission', 'Ironopolis\Course\Http\Controllers\CourseController@submission')->middleware('web');
Route::get('course/{course}', 'Ironopolis\Course\Http\Controllers\CourseController@find')->middleware('web');

Route::prefix('admin')->group(function () {
  //module admin routes
  Route::get('modules/import', 'Ironopolis\Course\Http\Controllers\ModuleAdminController@import')->middleware('web');
  Route::get('modules', 'Ironopolis\Course\Http\Controllers\ModuleAdminController@index')->middleware('web');
  Route::get('module/{module}', 'Ironopolis\Course\Http\Controllers\ModuleAdminController@show')->middleware('web');
  Route::get('module/builder', 'Ironopolis\Course\Http\Controllers\ModuleAdminController@build')->middleware('web');
  Route::put('module/builder/save', 'Ironopolis\Course\Http\Controllers\ModuleAdminController@save')->middleware('web');
  //import dates routes
  Route::get('/dates/import', 'Ironopolis\Course\Http\Controllers\CourseAdminController@getDatesImport')->middleware('web');
  Route::post('/dates/import', 'Ironopolis\Course\Http\Controllers\CourseAdminController@uploadDates')->middleware('web');
  //import courses routes
  Route::get('courses/import', 'Ironopolis\Course\Http\Controllers\CourseAdminController@getImport')->middleware('web');
  Route::post('courses/import', 'Ironopolis\Course\Http\Controllers\CourseAdminController@upload')->middleware('web');
  //course admin routes
  Route::get('courses', 'Ironopolis\Course\Http\Controllers\CourseAdminController@index')->middleware('web');
  Route::get('courses/search', 'Ironopolis\Course\Http\Controllers\CourseAdminController@search')->middleware('web');
  Route::get('course/builder', 'Ironopolis\Course\Http\Controllers\CourseAdminController@build')->middleware('web');
  Route::put('course/builder/save', 'Ironopolis\Course\Http\Controllers\CourseAdminController@save')->middleware('web');
  Route::get('course/{course}', 'Ironopolis\Course\Http\Controllers\CourseAdminController@show')->middleware('web');
});
